import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

//imports para funcionar
import { Observable } from 'rxjs-compat';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { User } from '../../models/user';

//fim

@Injectable()
export class AuthProvider {

  //atributo usuário que será usado para cadastro e autenticação
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth) {
      this.user = firebaseAuth.authState;
  }

  //metodo cadastro
  register(user: User) {
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
  }

  //método de login
  login(user: User) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  logout() {
    return this.firebaseAuth.auth.signOut();
  }

}
