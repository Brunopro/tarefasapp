import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// Imports adicionados
import { Observable } from 'rxjs/Observable';

// Serviço de tarefas
import { TarefasProvider } from '../../providers/tarefas/tarefas';

// Modelo de tarefas
import { Tarefas } from '../../models/tarefas'


@IonicPage()
@Component({
  selector: 'page-tarefas-finalizadas',
  templateUrl: 'tarefas-finalizadas.html',
})
export class TarefasFinalizadasPage {



  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TarefasFinalizadasPage');
  }

}
