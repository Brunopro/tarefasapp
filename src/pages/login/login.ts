import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../models/user';

//Para usar alert
import { AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { AuthProvider } from '../../providers/auth/auth';
//fim

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public user = {} as User;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              public auth: AuthProvider) {
  }



  //Para usar o Alert
  alert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  async login(user: User) {
    if(user.email == null || user.password == null){
      this.alert('Erro', 'É necessaio informar email e senha');
    } else {
      try {
        const result = await this.auth.login(user);
        if(result) {
          this.navCtrl.setRoot(TabsPage)
        }
      } catch (e) {
        this.alert('Erro', 'Não foi possivel concluir a operação')
      }
    }
  }
  async register(user: User) {

    if(user.email == null || user.password == null) {
      this.alert('Erro', 'É necessário informa o email e senha');
    } else {
      try{
        const result = await this.auth.register(user);
        if (result) {
          this.navCtrl.setRoot(TabsPage);
        }
      } catch (e) {
        this.alert('Erro ao cadastrar', e.message)
      }
    }
  }

  ionViewDidLoad() {
    // toda vez que um usuário acessar a página de login ele será deslogado
    this.auth.logout();
  }

}
